/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartping',
  version: '1.0.8',
  description: 'a ping utility'
}
