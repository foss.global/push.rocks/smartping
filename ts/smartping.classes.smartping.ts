import * as plugins from './smartping.plugins.js';

export class Smartping {
  public async ping(hostArg: string, timeoutArg: number = 500): Promise<plugins.ping.PingResponse> {
    const result = await plugins.ping.promise.probe(hostArg, {
      timeout: timeoutArg
    })
    return result;
  }

  public async pingAlive(hostArg: string, timeoutArg: number = 500): Promise<boolean> {
    const result = await plugins.ping.promise.probe(hostArg, {
      timeout: timeoutArg
    }).catch();
    //console.log(result);
    if (result.alive) {
      return true;
    } else {
      return false;
    }
  }
}