import { expect, expectAsync, tap } from '@pushrocks/tapbundle';
import * as smartping from '../ts/index.js';

let testPing: smartping.Smartping;

tap.test('should create an instance of Smartping', async () => {
  testPing = new smartping.Smartping();
  expect(testPing).toBeInstanceOf(smartping.Smartping);
});

tap.test('should deliver a ping result', async () => {
  const result = await testPing.ping('lossless.com', 1000);
  console.log(result);
})

tap.test('should detect alive', async () => {
  await expectAsync(testPing.pingAlive('lossless.com', 1000)).toBeTrue();
})

tap.start();
